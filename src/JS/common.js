/* film à l'affiche 1 */
let film1;
const divfilm1= document.getElementById("div-film1");
let divPoster = document.createElement('img');

window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=wonder+woman')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film1 = resultJson;
})
.then(() => {
    divPoster.setAttribute('src', film1.Poster);
    divfilm1.append(divPoster);
})
.then(() => {
    let p = document.createElement("p")
    divfilm1.append(p);
    p.innerHTML = film1.Title + "<br>" + film1.imdbRating + "<br>" + film1.Released;
    p.setAttribute('class', 'afficheinfo');
});

/* film à l'affiche 2 */
let film2;
const divfilm2= document.getElementById("div-film2");
let divPoster2 = document.createElement('img');

window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=dune')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film2 = resultJson;
})
.then(() => {
    divPoster2.setAttribute('src', film2.Poster);
    divfilm2.append(divPoster2);
})
.then(() => {
    let p = document.createElement("p")
    divfilm2.append(p);
    p.innerHTML = film2.Title + "<br>" + film2.imdbRating + "<br>" + film2.Released;
    p.setAttribute('class', 'afficheinfo')
});

/* film à l'affiche 3 */
let film3;
const divfilm3= document.getElementById("div-film3");
let divPoster3 = document.createElement('img');

window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=ghostbusters')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film3 = resultJson;
})
.then(() => {
    divPoster3.setAttribute('src', film3.Poster);
    divfilm3.append(divPoster3);
})
.then(() => {
    let p = document.createElement("p")
    divfilm3.append(p);
    p.innerHTML = film3.Title + "<br>" + film3.imdbRating + "<br>" + film3.Released;
    p.setAttribute('class', 'afficheinfo')
});

/* film à l'affiche 4 */
let film4;
const divfilm4= document.getElementById("div-film4");
let divPoster4 = document.createElement('img');

window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=joker')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film4 = resultJson;
})
.then(() => {
    divPoster4.setAttribute('src', film4.Poster);
    divfilm4.append(divPoster4);
})
.then(() => {
    let p = document.createElement("p")
    divfilm4.append(p);
    p.innerHTML = film4.Title + "<br>" + film4.imdbRating + "<br>" + film4.Released;
    p.setAttribute('class', 'afficheinfo');
});