
/*affichage info films interactif*/
const articlefilm0 = document.getElementById('article0');
const articlefilm1 = document.getElementById('article1');
const articlefilm2 = document.getElementById('article2');
const articlefilm3 = document.getElementById('article3');
const articlefilm4 = document.getElementById('article4');


/*film 1*/
let film11;
let divPoster11 = document.createElement('img');
window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=wonder+woman')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film11 = resultJson;
})
.then(() => {
    divPoster11.setAttribute('src', film11.Poster);
    articlefilm1.append(divPoster11)
})
.then(() => {
    let p = document.createElement("p")
    articlefilm1.append(p);
    p.innerHTML = "<h3>" + film11.Title + "</h3>" + film11.Released 
    + "<br>" + film11.imdbRating + " / " + film11.Rated 
    + "<br>" + film11.Plot + "<br>" + film11.Director + " / " + film11.Actors;
    p.setAttribute('class', 'afficheinfo')
});

/*film 2*/
let film22;
let divPoster22 = document.createElement('img');
window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=dune')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film22 = resultJson;
})
.then(() => {
    divPoster22.setAttribute('src', film22.Poster);
    articlefilm2.append(divPoster22)
})
.then(() => {
    let p = document.createElement("p")
    articlefilm2.append(p);
    p.innerHTML = "<h3>" + film22.Title + "</h3>" + film22.Released 
    + "<br>" + film22.imdbRating + " / " + film22.Rated 
    + "<br>" + film22.Plot + "<br>" + film22.Director + " / " + film22.Actors;
    p.setAttribute('class', 'afficheinfo')
});

/*film 3*/
let film33;
let divPoster33 = document.createElement('img');
window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=ghostbusters')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film33 = resultJson;
})
.then(() => {
    divPoster33.setAttribute('src', film33.Poster);
    articlefilm3.append(divPoster33)
})
.then(() => {
    let p = document.createElement("p")
    articlefilm3.append(p);
    p.innerHTML = "<h3>" + film33.Title + "</h3>" + film33.Released 
    + "<br>" + film33.imdbRating + " /" + film33.Rated 
    + "<br>" + film33.Plot + "<br>" + film33.Director + " / " + film33.Actors;
    p.setAttribute('class', 'afficheinfo')
});

/*film 4*/
let film44;
let divPoster44 = document.createElement('img');
window.fetch('http://www.omdbapi.com/?apikey=7d6cce15&t=joker')
.then(function (response){
    return response.json();
})
.then(function (resultJson) {
    film44 = resultJson;
})
.then(() => {
    divPoster44.setAttribute('src', film44.Poster);
    articlefilm4.append(divPoster44)
})
.then(() => {
    let p = document.createElement("p")
    articlefilm4.append(p);
    p.innerHTML = "<h3>" + film44.Title + "</h3>" + film44.Released 
    + "<br>" + film44.imdbRating + " / " + film44.Rated 
    + "<br>" + film44.Plot + "<br>" + film44.Director + " / " + film44.Actors;
    p.setAttribute('class', 'afficheinfo')
});


/* affichage dynamque des info de chaque film lorsqu'on clic sur l'affiche corespondante */
const affiche1 = document.getElementById('div-film1');
const affiche2 = document.getElementById('div-film2');
const affiche3 = document.getElementById('div-film3');
const affiche4 = document.getElementById('div-film4');


/*clic sur 1ere affiche*/
affiche1.addEventListener('click', function(){
    articlefilm1.style.display = 'flex';
    articlefilm2.style.display = 'none';
    articlefilm3.style.display = 'none';
    articlefilm4.style.display = 'none';
});

/*clic sur 2eme affiche*/
affiche2.addEventListener('click', function(){
    articlefilm1.style.display = 'none';
    articlefilm2.style.display = 'flex';
    articlefilm3.style.display = 'none';
    articlefilm4.style.display = 'none';
});

/*clic sur 3eme affiche*/
affiche3.addEventListener('click', function(){
    articlefilm1.style.display = 'none';
    articlefilm2.style.display = 'none';
    articlefilm3.style.display = 'flex';
    articlefilm4.style.display = 'none';
});

/*clic sur 4eme affiche*/
affiche4.addEventListener('click', function(){
    articlefilm1.style.display = 'none';
    articlefilm2.style.display = 'none';
    articlefilm3.style.display = 'none';
    articlefilm4.style.display = 'flex';
});